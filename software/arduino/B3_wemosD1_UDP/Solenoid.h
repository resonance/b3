#ifndef SOLENOID_H
#define SOLENOID_H

#include <Arduino.h>

class Solenoid
{
	public:
	Solenoid();
	~Solenoid();
	void init(int _pin, int _time_max, boolean _debug);
  void on(unsigned long _current, int _time);
  void off(unsigned long _current);
  boolean isOn();

	private:
	boolean debug;
  boolean state;
	unsigned long last;   
	long time_on; 
	int time_on_max;
	int pin;
};

#endif
