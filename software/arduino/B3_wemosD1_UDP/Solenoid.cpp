// Control Solenoid delay time

#include "Solenoid.h"

Solenoid::Solenoid(){
	state = false;
	last = 0;   
	time_on = 150;
	debug = false;
}

Solenoid::~Solenoid(){}

void Solenoid::init(int _pin, int _time_max, boolean _debug = false){
	debug = _debug;
	time_on_max = _time_max;
	pin = _pin;
  pinMode(pin, OUTPUT);
  digitalWrite(pin, LOW);
}

boolean Solenoid::isOn(){
  return state; 
}

void Solenoid::on(unsigned long _current, int _time){
	if( _current - last >= (time_on + 10) ) { // !state &&
    state = true;
    last = _current;
		time_on = constrain(_time, 0, time_on_max);
    digitalWrite(pin, HIGH);
    if (debug) Serial.println("ON ");
	}
}

void Solenoid::off(unsigned long _current) {
	if( _current - last >= time_on ) { // state &&
		state = false;
    digitalWrite(pin, LOW);
    if (debug) Serial.println("OFF");
	}
}
