/* 
 * Wemos D1 - Test output pins
 * LEDS drivers (inverse) : HIGH = OFF, LOW = ON
 * Analog output : 10 bits (0-1023)
 * 
 * Arduino  | Wemos Mapping
 * D0         3
 * D1         1
 * D2         16
 * D3         5
 * D4         4
 * D5/D13     14         
 * D6/D12     12
 * D7/D11     13
 * D8         0  (Pull-up)
 * D9         2  (Pull-up, buildin led)
 * D10        15 (Pull-down)    
 */

//const int pins[] = {0, 2, 15, 13, 12, 14}; // Arduino: D8, D9, D10, D11, D12, D13
//const int pins[] = {13, 12, 14}; // Arduino: D11, D12, D13
const int pins[] = {3, 15, 16, 13, 12, 14}; // Arduino: D8, D9, D10, D11, D12, D13
const int NB = 6;

void setup() {
  // SOLENOIDS OFF          
  for (int i = 0; i < NB - 3; i++) {
    pinMode(pins[i], OUTPUT);
    digitalWrite(pins[i], LOW); // OFF
  }

 // LEDs OFF
 for (int i = 3; i < NB; i++) {
    pinMode(pins[i], OUTPUT);
    digitalWrite(pins[i], HIGH); // OFF
  }
  // delay(500);
  // short time on startup : all on ?!
}

void loop() {
  // SOLENOIDS
  for (int i = 0; i < NB - 3; i++) {
    digitalWrite(pins[i], HIGH); // ON
    delay(50);
    digitalWrite(pins[i], LOW); // OFF
    delay(1000);
  }

  // LEDS
  for (int i = 3; i < NB; i++) {
    digitalWrite(pins[i], LOW); // ON
    delay(150);
    digitalWrite(pins[i], HIGH); // OFF
    delay(1000);
  }
  
    /*
     * LEDS
    analogWrite(pins[i], 0); // ON
    delay(1000);
    analogWrite(pins[i], 330);
    delay(1000);
    analogWrite(pins[i], 660);
    delay(1000);
    analogWrite(pins[i], 1023); // OFF
    delay(1000);
    */
}
