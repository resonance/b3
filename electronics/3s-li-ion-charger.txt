Description:

 

Module name: 3S 12.6V Li-ion Lithium Battery 18650 Charger Protection PCB Board

Suitable range: For nominal voltage 3.6V 3.7V lithium battery(Including 18650,26650, lithium polymer batteries)

Product size: 64*20*3.4mm

Product weight: 4.4g

Charging voltage: 12.6V

Maximum output current: 20A

Maximum output power / charging power: 252W

 

Precautions:

1.Strictly according to the diagram wiring: 0V/4.2V/8.4V/12.6V,Otherwise it will cause damage to the chip.

2.After connection,it need to first charge activation, then will have the output.

 

Note: Do not mix the good battery and poor battery to use.The internal resistance of 3 battery capacity are closer will be better.Please buyers know the professional knowledge of this module before buying